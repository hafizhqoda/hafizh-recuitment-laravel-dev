<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->

<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Soal Nomor 1</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    </head>

    <body>
        <section class="container">
            <h2 class="mt-4">Soal 1</h2>

            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            A.i Convert String
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <div class="pt-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="inputString" placeholder="Input String"
                                        name="convertstring" value="">
                                    <label for="inputString">Input String</label>
                                </div>
                                <button class="btn btn-primary mt-3" id="convert">Convert</button>
                            </div>
                            <div class="mt-3">
                                <label for="result">Result</label>
                                <h4 id="result" class="fw-bold"></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            A.ii Count Character
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <div class="pt-3">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="input2" placeholder="Input String"
                                        name="countstring" value="">
                                    <label for="inputString">Input Character</label>
                                </div>
                                <button class="btn btn-primary mt-3" id="count">Count</button>
                            </div>
                            <div class="mt-3">
                                <label>Result</label>
                                <p id="result2" class="fw-bold">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            B. Aritmatika
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p id='datas'></p>
                            <div class="mt-3">
                                <button class="btn btn-primary" id="hitung">Hitung</button>
                                <div class="row d-flex flex-column mt-3">
                                    <div class="col-md-12">
                                        <h4 id="total_saldo"> Hitung Total :</h4>
                                        <h4 class="fw-bold" id="r_total_saldo"></h4>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 id="max_saldo"> Maximal Saldo :</h4>
                                        <h4 class="fw-bold" id="r_max_saldo"></h4>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 id="min_saldo"> Minimum Saldo :</h4>
                                        <h4 class="fw-bold" id="r_min_saldo"></h4>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 id="rata_rata"> Rata-rata :</h4>
                                        <h4 class="fw-bold" id="r_rata_saldo"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- <script src="https://code.jquery.com/jquery-3.7.0.slim.js" -->
        <!-- integrity="sha256-7GO+jepT9gJe9LB4XFf8snVOjX3iYNb0FHYr5LI1N5c=" crossorigin="anonymous"></script> -->
        <script src="https://code.jquery.com/jquery-3.7.0.min.js"
            integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
            integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"
            integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
        </script>

        <script>
        // 1. i
        $('#convert').on('click', function() {
            var value = document.getElementById('inputString').value.toLowerCase();
            const arr = value.split("").sort((a, b) => isFinite(a) - isFinite(b) || a > b || -(a <
                    b))
                .join('');

            console.log(arr);
            document.getElementById('result').innerHTML = arr;

        });


        // 1. ii
        $('#count').on('click', function() {
            let test_str = document.getElementById('input2').value;

            if (test_str.length == 0)
                alert('input character first')

            const con = Char_Counts(test_str);

            document.getElementById('result2').innerHTML = JSON.stringify(con);

        });

        function Char_Counts(str1) {
            // Get all Value 
            var ka = {}
            for (let x = 0; x < str1.length; x++) {
                var l = str1.charAt(x);
                ka[l] = (isNaN(ka[l]) ? 1 : ka[l] + 1)
            }
            return ka;
        }

        // 2.
        let arr = [{
                'id': '1',
                'name': 'Hafizh Qodarisman',
                'saldo': 10
            },
            {
                'id': '2',
                'name': 'Tukang Bakwan',
                'saldo': 110
            },


        ];
        document.getElementById('datas').innerHTML = JSON.stringify(arr);
        // console.log(arr);

        $('#hitung').on('click', function() {
            var total = 0;
            var max_saldo = 0;
            var min_saldo = 0;
            for (let i = 0; i < arr.length; i++) {
                total += arr[i].saldo;
                var nilai = total / arr.length
                if (i == 0) {
                    max_saldo = arr[i];
                    min_saldo = arr[i];
                } else {
                    if (arr[i].saldo > max_saldo.saldo) {
                        max_saldo = arr[i];
                    }
                    if (arr[i].saldo < min_saldo.saldo) {
                        min_saldo = arr[i];
                    }
                }
            }

            // console.log('total Saldo = ' + total);
            document.getElementById('r_total_saldo').innerHTML = 'Rp. ' +
                total;
            document.getElementById('r_max_saldo').innerHTML = 'Saldo Maximum = Rp. ' + max_saldo.saldo +
                '<br\>Name = ' +
                max_saldo.name;
            document.getElementById('r_min_saldo').innerHTML = 'Saldo Minimum = Rp. ' + min_saldo.saldo +
                '<br\>Name = ' +
                min_saldo.name;
            document.getElementById('r_rata_saldo').innerHTML = 'Rp. ' + nilai;

        });
        </script>


    </body>

</html>