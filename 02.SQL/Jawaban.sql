

-- 1
 SELECT "status", SUM("nominal") FROM "transaction" GROUP BY "status" LIMIT 50

 -- 2
SELECT "status", AVG("nominal") FROM "transaction" GROUP BY "status" LIMIT 50

--3
SELECT COUNT("userId"), "userId" FROM "merchants" GROUP BY "userId" LIMIT 50

--4
SELECT "merchants"."name:", "transaction"."date_transaction", "transaction"."nominal", "transaction"."status" FROM "merchants" 
INNER JOIN "transaction" on "merchants"."id" = "transaction"."merchantsId"

