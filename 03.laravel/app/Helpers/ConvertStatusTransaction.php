<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class ConvertStatusTransaction
{
    public static function p_status($x)
    {
        switch ($x) {
            case 'Success':
                $x = 'S';
                break;
            case 'Failed':
                $x = 'F';
                break;
            case 'Pending':
                $x = 'P';
                break;
            case 'Refund':
                $x = 'R';
                break;
            default:
                break;
        }
        return $x;
    }

    public static function f_status($x)
    {
        switch ($x) {
            case 'S':
                $x = 'Success';
                break;
            case 'F':
                $x = 'Failed';
                break;
            case 'P':
                $x = 'Pending';
                break;
            case 'R':
                $x = 'Refund';
                break;
            default:
                break;
        }
        return $x;
    }

}
?>