<?php

namespace App\Models\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Model_has_Permissions extends Model
{
    use HasFactory;

    protected $table = 'model_has_role';

    function Users_has_roles()
    {
        return $this->belongsTo(User::class, 'model_id', 'id');
    }
    function Roles()
    {
        return $this->belongsTo(Role::class);
    }
}