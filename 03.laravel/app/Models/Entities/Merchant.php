<?php

namespace App\Models\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Merchant extends Model
{
    use HasFactory, HasRoles;

    protected $table = 'merchants';

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'merchantsId', 'id');
    }

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'userId');
    }
}