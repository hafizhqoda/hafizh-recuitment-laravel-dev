<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Enum\StatusTransaction;

class Transaction extends Model
{
    use HasFactory, HasRoles;

    protected $table = 'transaction';

    protected $fillable = [
        'merchantsId',
        'nominal',
        'status'
    ];

    public function merchants()
    {
        return $this->hasMany(Merchant::class, 'id', 'merchantsId');
    }
}