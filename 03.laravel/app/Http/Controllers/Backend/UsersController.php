<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Arr;
use DB;
use Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:users-list|users-create|users-edit|users-delete', ['only' => ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy']]);
        $this->middleware('permission:users-create', ['only' => ['show', 'store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        //
        // $datas = User::select('*')->with('model_has_role')->get();


        if ($request->ajax()) {
            $user = User::latest();

            return Datatables::of($user)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    // Update Button
                    $updateButton = "<a href='" . route('edit_users', $row->id) . "' target='__blank' class='btn btn-sm btn-info updateUser' ><i class='bi bi-pencil-square'></i></a>";

                    // Delete Button
                    $deleteButton = "<a href='javascript:void(0);' class='btn btn-sm btn-danger deleteUser' onclick='deleteData(" . $row->id . ")'><i class='bi bi-trash'></i></a>";

                    return $updateButton . " " . $deleteButton;
                })
                ->rawColumns(['action'])
                ->make(true);

        }
        // dd($datas);
        return view('backend.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        // dd($roles);
        return view('backend.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm_password',
            'roles' => 'required',
        ]);

        $input = $request->all();
        $input['email_verified_at'] = \Carbon\Carbon::now();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));



        return redirect()->route('users.index')->with('success', 'User created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $roles = Role::pluck('name', 'name')->all();
        $user = User::find($id);

        if ($user == NULL) {
            return redirect()->route('users.index');
        }
        $permission = Permission::get();
        $rolePermissions = DB::table('model_has_roles')->join('role_has_permissions', 'role_has_permissions.role_id', '=', 'model_has_roles.role_id')->select('role_has_permissions.permission_id')->where('model_has_roles.model_id', $id)->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')->all();
        // $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('backend.users.edit', compact('user', 'permission', 'rolePermissions', 'userRole', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['updated_at'] = \Carbon\Carbon::now();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id)->delete();
        if ($data) {
            return response()->json(array('success' => true));
        } else {
            return response()->json(array('error' => true));
        }
    }
}