<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ConvertStatusTransaction;
use App\Http\Controllers\Controller;
use App\Models\Entities\Merchant;
use App\Models\Entities\Transaction;
use App\Models\Entities\StatusModel;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:transaction-list|transaction-create|transaction-edit|transaction-delete', ['only' => ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy']]);
        $this->middleware('permission:transaction-create', ['only' => ['show', 'store']]);
        $this->middleware('permission:transaction-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:transaction-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        //

        // dd($datas);
        if ($request->ajax()) {
            $datas = Merchant::select('*')->with('transaction', 'users')->where('userId', Auth::user()->id)->get();

            return DataTables::of($datas)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    // Update Button
                    $updateButton = "<a href='" . route('edit_transaction', $row->id) . "' target='__blank' class='btn btn-sm btn-info updateUser' ><i class='bi bi-pencil-square'></i></a>";

                    // Delete Button
                    $deleteButton = "<a href='javascript:void(0);' class='btn btn-sm btn-danger deleteUser' onclick='deleteData(" . $row->id . ")'><i class='bi bi-trash'></i></a>";

                    return $updateButton . " " . $deleteButton;
                })
                ->rawColumns(['action'])
                ->make(true);

        }
        return view('backend.transaction.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $status = StatusModel::all();
        // dd($status);
        return view('backend.transaction.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $nominal = str_replace(".", "", str_replace("Rp. ", "", $request->nominal));
        $status = ConvertStatusTransaction::p_status($request->status);
        // dd($status);
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'nominal' => 'required',
            'status' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $data = new Merchant;
            $data->name = $request->name;
            $data->alamat = $request->address;
            $data->userId = Auth::user()->id;
            $data->save();

            if ($data) {
                $dataTransaction = new Transaction;
                $dataTransaction->nominal = $nominal;
                $dataTransaction->merchantsId = $data->id;
                $dataTransaction->status = $status;
                $dataTransaction->date_transaction = $request->date;
                $dataTransaction->save();
                // dd($dataTransaction);

                DB::commit();
                return redirect()->route('transaction.index')->with('success', 'Data has been created');

            } else {
                DB::rollBack();

                // echo "error 1";
                return redirect()->route('transaction.create')->with('error', 'Ups Error 1.... Please check your data against!');
            }
        } catch (Exception $e) {
            DB::rollBack();

            // dd("error 2 " . $e);
            return redirect()->route('transaction.create')->with('error', 'Ups Error 2... Please check your data against!');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            // dd("error 3 " . $e);
            return redirect()->route('transaction.create')->with('error', 'Ups Error 3.... Please check your data against!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $datas = Merchant::select('*')->with('transaction', 'users')->where('id', $id)->first();
        $datas->transaction->status = ConvertStatusTransaction::f_status($datas->transaction->status);
        $status = StatusModel::all();
        return view('backend.transaction.edit', compact('status', 'datas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $nominal = str_replace(".", "", str_replace("Rp. ", "", $request->nominal));
        $status = ConvertStatusTransaction::p_status($request->status);
        $transactionID = Transaction::where('merchantsId', $id)->pluck('id')->first();
        // dd($status);
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'nominal' => 'required',
            'status' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $data = Merchant::findorfail($id);
            $data->name = $request->name;
            $data->alamat = $request->address;
            $data->userId = Auth::user()->id;
            $data->save();

            if ($data) {
                $dataTransaction = Transaction::findorfail($transactionID);
                $dataTransaction->nominal = $nominal;
                $dataTransaction->merchantsId = $data->id;
                $dataTransaction->status = $status;
                $dataTransaction->date_transaction = $request->date;
                $dataTransaction->save();
                // dd($dataTransaction);

                DB::commit();
                return redirect()->route('transaction.index')->with('success', 'Data has been updated');

            } else {
                DB::rollBack();

                // echo "error 1";
                return redirect()->route('edit_transaction', $id)->with('error', 'Ups Error 1.... Please check your data against!');
            }
        } catch (Exception $e) {
            DB::rollBack();

            // dd("error 2 " . $e);
            return redirect()->route('edit_transaction', $id)->with('error', 'Ups Error 2... Please check your data against!');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            // dd("error 3 " . $e);
            return redirect()->route('edit_transaction', $id)->with('error', 'Ups Error 3.... Please check your data against!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // dd($id);

        $transactionID = Transaction::where('merchantsId', $id)->pluck('id')->first();
        $data = Merchant::find($id)->delete();

        if ($data) {
            Transaction::findorfail($transactionID)->delete();
            return response()->json(array('success' => true));
        } else {
            return response()->json(array('error' => true));
        }
    }
}