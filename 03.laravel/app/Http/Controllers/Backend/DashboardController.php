<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ConvertStatusTransaction;
use App\Http\Controllers\Controller;
use App\Models\Entities\Merchant;
use App\Models\Entities\StatusModel;
use App\Models\Entities\Transaction;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $users = Auth::user();

        $data['count_amount'] = $this->IDR(Transaction::sum('nominal'));
        $data['avg_amount'] = $this->IDR(Transaction::sum('nominal'));
        $status = StatusModel::all();
        $db_status2_sum = Transaction::select('status', DB::raw('SUM(nominal) as total'))->groupBY('status')->get();
        $db_status2_avg = Transaction::select('status', DB::raw('AVG(nominal) as avg'))->groupBY('status')->get();

        foreach ($status as $stts) {
            $stts->new_name = ConvertStatusTransaction::p_status($stts->name);
            foreach ($db_status2_sum as $item) {
                if ($stts->new_name == $item->status) {
                    $stts['total'] = $this->IDR($item->total);
                }
            }
            foreach ($db_status2_avg as $item) {
                if ($stts->new_name == $item->status) {
                    $stts['avg'] = $this->IDR($item->avg);
                }
            }

        }
        if ($request->ajax()) {

            $date = Carbon::now()->format('Y-m-d');
            $datas = Merchant::select('*')->with('transaction')->whereHas('transaction', function ($q) use ($date) {
                // Query the name field in status table
                $q->where('date_transaction', '=', $date); // '=' is optional
            })->get();
            return DataTables::of($datas)
                ->addIndexColumn()
                ->addColumn('user', function ($datas) {
                    $users = User::all();
                    foreach ($users as $user) {
                        if ($datas['userId'] == $user->id) {
                            $getName = $user->name;
                        }
                    }
                    return $getName;
                })
                ->addColumn('status', function ($datas) {
                    $newStatus = ConvertStatusTransaction::f_status($datas->transaction->status);
                    return $newStatus;
                })
                ->rawColumns(['user'])
                ->make(true);
        }

        return view('backend.dashboard.index', compact('users', 'data', 'status'));
    }

    private function IDR($angka)
    {
        $hasil_rupiah = "IDR " . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;

    }
}