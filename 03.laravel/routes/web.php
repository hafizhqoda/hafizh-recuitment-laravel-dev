<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index')->name('login')->middleware('guest');

Route::post('authenticate', 'LoginController@loginForm')->name('Authenticate');
Route::post('logout', 'LoginController@logout')->name('logout');


Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', 'Backend\DashboardController@index')->name('dashboard');
    // Route::resource('dashboard', 'Backend\DashboardController');

    Route::resource('users', 'Backend\UsersController');
    Route::get('users/edit/{id}', 'Backend\UsersController@show')->name('edit_users');
    Route::patch('users/update/{id}', 'Backend\UsersController@update')->name('update_users');
    Route::delete('users/destroy/{id}', 'Backend\UsersController@destroy')->name('destroy_users');

    Route::resource('transaction', 'Backend\TransactionController');
    Route::get('transaction/edit/{id}', 'Backend\TransactionController@show')->name('edit_transaction');
    Route::patch('transaction/update/{id}', 'Backend\TransactionController@update')->name('update_transaction');
    Route::delete('transaction/destroy/{id}', 'Backend\TransactionController@destroy')->name('destroy_transaction');


});