<?php

namespace Database\Seeders;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;

class statusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('status')->insert(
            [
                ['name' => 'Success', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                ['name' => 'Failed', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                ['name' => 'Refund', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                ['name' => 'Pending', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ]
        );
    }
}