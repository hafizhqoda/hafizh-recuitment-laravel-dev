<?php

namespace Database\Seeders;

use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'email_verified_at' => \Carbon\Carbon::now(),
        ]);

        $admin->assignRole('Admin');

        $operator = User::create([
            'name' => 'Operator',
            'email' => 'operator@operator.com',
            'password' => Hash::make('password'),
            'email_verified_at' => \Carbon\Carbon::now(),
        ]);

        $operator->assignRole('Operator');

        $merchant = User::create([
            'name' => 'Merchant',
            'email' => 'merchant@merchant.com',
            'password' => Hash::make('password'),
            'email_verified_at' => \Carbon\Carbon::now(),
        ]);

        $merchant->assignRole('Merchant');

    }
}