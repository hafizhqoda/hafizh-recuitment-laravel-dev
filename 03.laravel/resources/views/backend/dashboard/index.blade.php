@extends('layout.backend')
@section('content')



<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mb-5">
    <div class="pt-3 pb-3 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <h3 class="h4">Welcome, {{$users->name}}</h3>
    </div>
    <div class="container">
        <div class="d-flex mb-5">
            @foreach($status as $stts)
            <div class=" card me-3" style="width:12rem">
                <div class="card-body d-flex flex-column justify-content-around">
                    <h5 class="card-title">{{$stts->name}} Total Transaction Amount</h5>
                    <p class="h1 card-text">{{$stts->total != '' ? $stts->total : '0'}}</p>
                </div>
            </div>
            @endforeach
        </div>
        <div class="d-flex mb-5">
            @foreach($status as $stts)
            <div class=" card me-3" style="width:12rem">
                <div class="card-body d-flex flex-column justify-content-around">
                    <h5 class="card-title">{{$stts->name}} Avg Transaction Amount</h5>
                    <p class="h1 card-text">{{$stts->avg != '' ? $stts->avg : '0'}}</p>
                </div>
            </div>
            @endforeach
        </div>

        <div class="mt-4">
            <h3>Transaction Today</h3>
            <table id="data-table" class="table display w-100">
                <thead class="text-center pt-4">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Nominal</th>
                        <th scope="col">Status</th>
                        <th scope="col">Tanggal</th>
                        <!-- <th scope="col">Roles</th> -->
                        <th scope="col">Merchant</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>


</main>
@endsection
@section('script')
<script>
$(document).ready(function() {
    var table = $('#data-table').DataTable({
        bFilter: false,
        stateSave: true,
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: "{{ route('dashboard') }}",
        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'name',
            },
            {
                data: 'alamat',
            },
            {
                data: 'transaction.nominal',
            },
            {
                data: 'status',
            },
            {
                data: 'transaction.date_transaction',
            },
            {
                data: 'user'
            }
        ]
    });
});
</script>
@endsection