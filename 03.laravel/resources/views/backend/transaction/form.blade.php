@if(session()->has('error'))
{{session('error')}}
@enderror
<div class="row g-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="floatingName"
                placeholder="Jhon Cena" required value="<?= isset($datas) == 0 ? old('name') : $datas->name?>">
            <label for="floatingName">Name</label>
        </div>
    </div>
    <div class="col-md">
        <div class="form-floating">
            <input type="text" step="0.01" class="form-control @error('nominal') is-invalid @enderror"
                id="nominalFloating" name="nominal" placeholder="Nominal"
                value="<?= isset($datas) == 0 ? old('name') : $datas->transaction->nominal?>">
            <label for="nominalFloating">Nominal</label>
            @error('nominal')
            <div class="invalid-feedback">
                Input Nominal
            </div>
            @enderror
        </div>
    </div>
</div>
</div>
<div class="row g-2 my-3">
    <div class="col-md">
        <div class="form-floating">
            <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2"
                style="height: 100px" name="address"><?= isset($datas) == 0 ? old('name') : $datas->alamat?></textarea>
            <label for="floatingTextarea2">Address</label>
        </div>
        @error('address')
        <div class="invalid-feedback">
            Invalid Address
        </div>
        @enderror
    </div>

</div>
<div class="row g-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="date" class="form-control  @error('date') is-invalid @enderror" id="dateFloating" name="date"
                value="<?= isset($datas) == 0 ? old('name') : $datas->transaction->date_transaction?>">
            <label for="dateFloating">Date</label>
            @error('date')
            <div class="invalid-feedback">
                input date.
            </div>
            @enderror
        </div>
    </div>
    <div class="col-md">
        <div class="form-floating">
            <select class="form-select" id="floatingSelectGrid" aria-label="Floating label select example" name="status"
                required>
                <option selected disabled>Select Status</option>
                @foreach($status as $value)
                <option value='{{$value["name"]}}' <?= isset($datas->transaction->status) ?
                $value->name == $datas->transaction->status ? 'selected' : '' : ''?>>{{$value->name}}</option>
                @endforeach
            </select>
            <label for="floatingSelectGrid">Status</label>
        </div>
    </div>
</div>
<div class="mt-3 d-flex justify-content-start align-items-start">
    <button class="btn btn-primary" type="submit"><i class="bi bi-save"></i> Save Data</button>
</div>