@extends('layout.backend')
@section('css')
<style>
.kotak {
    width: 350px;
    margin: auto;
    margin-top: 15px;
    padding: 10px;
}

p {
    margin-bottom: 20px;
    color: #0004ff;
}

input[type::number] {
    text-align: right;
    width: 100%;
    margin-bottom: 20px;
    margin-top: 10px;
    padding: 7px 10px;
    font-size: 18px;
}
</style>
@endsection
@section('content')


<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Transaction</h1>
        <div cl ass="btn-toolbar mb-2 mb-md-0">
            <a href="{{route('transaction.index')}}" type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="arrow-left"></span>

                Back
            </a>
        </div>
    </div>
    <div class="">
        <form method="post" action="{{route('update_transaction',$datas->id)}}">
            @csrf
            @method('PATCH')
            <div>
                @include('backend.transaction.form')
            </div>
        </form>
    </div>

</main>
@endsection
@section('script')
<script>
var rupiah = document.getElementById('nominalFloating');
rupiah.addEventListener('keyup', function(e) {
    // tambahkan 'Rp.' pada saat form di ketik
    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    rupiah.value = formatRupiah(this.value, 'Rp. ');

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
});
</script>
@endsection