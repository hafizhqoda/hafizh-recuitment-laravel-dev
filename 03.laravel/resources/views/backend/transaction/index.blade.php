@extends('layout.backend')
@section('content')

<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">transaction Page</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{route('transaction.create')}}" type="button" class="btn btn-sm btn-secondary">
                <span data-feather="user-plus"></span>
                Add Data
            </a>
        </div>
    </div>

    <div class="container">
        @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
            <button class="btn-close" type="button" data-bs-dismiss="alert" arial-label="Close"></button>
        </div>
        @endif
        <table id="data-table" class="table display w-100">
            <thead class="text-center pt-4">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Nominal</th>
                    <th scope="col">Status</th>
                    <th scope="col">Tanggal</th>
                    <!-- <th scope="col">Roles</th> -->
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</main>
@endsection
@section('script')
<script>
$(document).ready(function() {
    var table = $('#data-table').DataTable({
        stateSave: true,
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: "{{ route('transaction.index') }}",
        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'name',
            },
            {
                data: 'alamat',
            },
            {
                data: 'transaction.nominal',
            },
            {
                data: 'transaction.status',
            },
            {
                data: 'transaction.date_transaction',
            },
            {
                data: 'action'
            }
        ]
    });
});

function deleteData(id) {
    let url = id;
    console.log(url);
    Swal.fire({
        title: 'Are you sure you want to delete this record?',
        text: 'If you delete this, it will be gone forever.',
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        buttonsStyling: true,
    }).then(function(e) {
        if (e.value === true) {
            $.ajax({
                type: "DELETE",
                url: `transaction/destroy/${url}`,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    if (data.success === true) {
                        Swal.fire('Deleted!', 'Delete Data Successfully!.', "success")
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    } else {
                        swal.fire("Error!", 'Delete Data Failed', "error");
                    }
                }
            });
        } else {
            e.dismiss;
        }
    }, function(dismiss) {
        return false;
    });
}
</script>
@endsection