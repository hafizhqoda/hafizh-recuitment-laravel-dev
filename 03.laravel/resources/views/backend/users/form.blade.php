<div class="row g-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="floatingName"
                placeholder="Jhon Cena" required value="<?= isset($user) == 0 ? old('name') : $user['name']?>">
            <label for="floatingName">Name</label>
        </div>
    </div>
    <div class="col-md">
        <div class="form-floating">
            <input type="email" class="form-control @error('email') is-invalid @enderror" id="floatingEmail"
                placeholder="name@example.com" name="email" required
                value="<?= isset($user) == 0 ? old('email') : $user['email']?>">
            <label for="floatingEmail">Email address</label>
            @error('label')
            <div class="invalid-feedback">
                Invalid Email
            </div>
            @enderror
        </div>
    </div>
</div>
<div class="row g-2 my-3">
    <div class="col-md">
        <div class="form-floating">
            <input type="password" class="form-control @error('password') is-invalid @enderror" id="passwordFloating"
                name="password" placeholder="type your password">
            <label for="passwordFloating">Password</label>
            @error('password')
            <div class="invalid-feedback">
                Password do not match.
            </div>
            @enderror
        </div>
    </div>
    <div class="col-md">
        <div class="form-floating">
            <input type="password" class="form-control  @error('confirm_password') is-invalid @enderror"
                id="passwordFloating2" name="confirm_password" placeholder="Re-type your password">
            <label for="passwordFloating2">Confirm Password</label>
            @error('confirm_password')
            <div class="invalid-feedback">
                Password do not match.
            </div>
            @enderror
        </div>
    </div>
</div>
<div class="row g-2">
    <div class="col-md-3">
        <div class="form-floating">
            <select class="form-select" id="SelectRole" aria-label="Floating label select example" name="roles"
                required>
                <option selected disabled>Select Roles</option>
                @foreach($roles as $role)
                <option value='{{$role}}' <?= isset($userRole) ?
                $role == $userRole ? '' : 'selected' : ''?>>{{$role}}</option>
                @endforeach
            </select>
            <label for="SelectRole">Role</label>
        </div>
    </div>
</div>

<div class="mt-3 d-flex justify-content-end align-items-end">
    <button class="btn btn-primary" type="submit"><i class="bi bi-save"></i> Save Data</button>
</div>