@extends('layout.backend')
@section('content')


<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div
        class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit User</h1>
        <div cl ass="btn-toolbar mb-2 mb-md-0">
            <a href="{{route('users.index')}}" type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="arrow-left"></span>

                Back
            </a>
        </div>
    </div>
    <div class="">
        <form method="post" action="{{route('update_users',$user->id)}}">
            @csrf
            @method('PATCH')
            <div>
                @include('backend.users.form')
            </div>
        </form>
    </div>

</main>
@endsection
@section('script')
<script>

</script>
@endsection