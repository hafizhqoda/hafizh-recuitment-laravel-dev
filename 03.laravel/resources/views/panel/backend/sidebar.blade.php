<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'dashboard') ? 'active' : 'none' ?>" aria-current="page"
                    href="{{route('dashboard')}}">
                    <span data-feather="home"></span>
                    Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= (Request::segment(1) == 'transaction') ? 'active' : 'none' ?>"
                    href="{{route('transaction.index')}}">
                    <span data-feather="bar-chart-2"></span>
                    Transaction
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Management</span>
            <a class="link-secondary" href="#" aria-label="Manage Users">
                <i class="bi bi-gear"></i>
            </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item ">
                <a class="nav-link <?= (Request::segment(1) == 'users') ? 'active' : 'none' ?>"
                    href="{{route('users.index')}}">
                    <span data-feather="users"></span>
                    Users
                </a>
            </li>
        </ul>
    </div>
</nav>